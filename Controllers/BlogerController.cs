﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CSBlogModel;
using CSBlogServices.Core;
using CSBlogServices.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CSBlogApi.Controllers
{
    [Route("api/[controller]/[action]")]
    public class BlogerController : Controller
    {
        private readonly BlogerRespository blogerService;
        private readonly SubscribeRepository subscribeService;

        public BlogerController(BlogerRespository blogerRespository, SubscribeRepository subscribeService)
        {
            this.blogerService = blogerRespository;
            this.subscribeService = subscribeService;
        }

        // no authen
        [HttpGet]
        public IActionResult Get()
        {
            IQueryable<Bloger> blogers = blogerService.GetAll();
            return Ok(new Result
            {
                Status = true,
                Message = "Success",
                Data = blogers
            });
        }

        // no authen
        [HttpGet("{blogerId}")]
        public IActionResult GetByID(Guid blogerId)
        {
            Bloger bloger = blogerService.GetByID(blogerId);// ยังไม่ includeå
            return Ok(new Result
            {
                Status = true,
                Message = "Success",
                Data = bloger
            });
        }

        [HttpPost]
        public IActionResult Insert([FromBody]ProfileRes profile)
        {
            try
            {
                Bloger bloger = new Bloger();
                bloger.Profile.UpdateProfile(profile.CodeName, profile.Bio, profile.Description);
                blogerService.Add(bloger);

                return Ok(new Result
                {
                    Status = true,
                    Message = "create success",
                    Data = bloger
                });
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        [HttpGet("{blogerId}")]
        public IActionResult GetFollowingById(Guid blogerId)
        {
            var bloger = blogerService.Find(blogerId);
            if (bloger == null) throw new Exception("not found bloger");
            Bloger res = blogerService.GetFollow(bloger.BlogerID);
            return Ok(res);
        }

        [HttpPost]
        public IActionResult AddFollow([FromBody]SubscribeRes follow)
        {
            var following = blogerService.Find(follow.FollowID);
            var bloger = blogerService.Find(follow.BlogerID);

            if (bloger == null) throw new Exception("bloger not found");
            if (following == null) throw new Exception("following not found");

            Subscribe subscribe = new Subscribe();
            subscribe.FollowBloger(bloger.BlogerID, following.BlogerID);

            Subscribe res = subscribeService.Add(subscribe);
            return Ok(res);
        }

        [HttpPost]
        public IActionResult RemoveFollow([FromBody]SubscribeRes follow)
        {
            Subscribe subscribe = new Subscribe();
            subscribe.FollowBloger(follow.BlogerID, follow.FollowID);
            Subscribe res = subscribeService.Add(subscribe);
            return Ok(res);
        }
    }
}
