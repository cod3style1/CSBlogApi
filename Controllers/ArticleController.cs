﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CSBlogModel;
using CSBlogServices.Core;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CSBlogApi.Controllers
{
    [Route("api/[controller]/[action]")]
    public class ArticleController : ControllerBase
    {
        private readonly ArticleRespository articleService;

        public ArticleController(ArticleRespository articleService)
        {
            this.articleService = articleService;
        }

        // no authen
        [HttpGet]
        public IActionResult Get()
        {
            IQueryable<Article> res = articleService.All;
            return Ok(new Result {
                Status = true,
                Message = "success",
                Data = res
            });
        }

        // no authen
        [HttpGet("{articleID}")]
        public IActionResult GetByID(Guid articleID)
        {
            Article res = articleService.Find(articleID);
            return Ok(new Result
            {
                Status = true,
                Message = "success",
                Data = res
            });
        }

        [HttpPost]
        public IActionResult Add([FromBody]AddArticleRes articleRes)
        {

            Article article = new Article(articleRes.Title, articleRes.Tages, articleRes.SubTitle);
            article.IsBloger(articleRes.BlogerID);

            //article.UpdateParagraphs(articleRes.Paragraphs);
            Article res = articleService.Add(article);
            return Ok(new Result
            {
                Status = true,
                Message = "success",
                Data = res
            });
        }
    }
}
