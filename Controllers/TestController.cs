﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CSBlogServices.Core;
using CSBlogServices.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CSBlogApi.Controllers
{
    //[Route("api/[controller]/[action]")]
    public class TestController : ControllerBase
    {
        private readonly AppDB dB;

        public TestResposity TestResposity { get; }

        public TestController(TestResposity testResposity, AppDB dB)
        {
            TestResposity = testResposity;
            this.dB = dB;
        }

        [HttpGet]
        public IActionResult GetStudentOdd()
        {
            List<Room> list = dB.Room.Include(it => it.Students).ToList();
            return Ok(list);
        }

        [HttpGet]
        public IActionResult GetStudent()
        {
            var res = TestResposity.All.ToList();
            if (res == null) return BadRequest();

            //List<Student> list = dB.Student.Include(it => it.Room).ToList();
            return Ok(res);
        }

        [HttpPost]
        public IActionResult AddRoomOdd([FromBody]Room room)
        {
            dB.Room.Add(room);
            dB.SaveChanges();
            return Ok(room);
        }


        [HttpPost]
        public IActionResult AddStudentOdd([FromBody]Student room)
        {
            dB.Student.Add(room);
            dB.SaveChanges();
            return Ok(room);
        }

        [HttpPost]
        public IActionResult AddStudent([FromBody]Student student)
        {
            Student s = new Student();

            s.Name = student.Name;
            s.Age = student.Age;
            s.RoomID = student.RoomID;

            var res = TestResposity.Add(s);
            return Ok(res);
        }
    }
}
