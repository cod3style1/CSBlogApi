﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CSBlogModel;
using CSBlogServices.Core;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CSBlogApi.Controllers
{
    [Route("api/[controller]/[action]")]
    public class ProfileController : ControllerBase
    {
        private readonly ProfileRespository profileService;

        public ProfileController(ProfileRespository profileService)
        {
            this.profileService = profileService;
        }

        [HttpGet("{profileID}")]
        public IActionResult GetById(Guid profileID)
        {
            Profile profile = profileService.Find(profileID);
            return Ok(new Result
            {
                Status = true,
                Message = "success",
                Data = profile
            });
        }

        [HttpPut]
        public IActionResult Update([FromBody]ProfileRes profileUpdate)
        {
            Profile profile = profileService.UpdateProfile(profileUpdate);

            return Ok(new Result
            {
                Status = true,
                Message = "success",
                Data = profile
            });
        }
    }
}
