﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CSBlogModel;
using CSBlogServices.Core;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CSBlogApi.Controllers
{
    [Route("api/[controller]/[action]")]
    public class TagController : ControllerBase
    {
        private readonly TagRepository tagService;

        public TagController(TagRepository tagService)
        {
            this.tagService = tagService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var res = tagService.All;
            return Ok(new Result
            {
                Status = true,
                Message = "success",
                Data = res
            });
        }

        [HttpGet("{tagId}")]
        public IActionResult GetById(Guid tagId)
        {
            var res = tagService.GetArticleByTag(tagId);
            return Ok(new Result
            {
                Status = true,
                Message = "success",
                Data = res
            });
        }
    }
}
