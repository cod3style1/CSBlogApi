﻿using System;
using CSBlogServices.Core;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;

namespace CSBlogApi.Helpers
{
    public static class ConfigurationSettings
    {
        public static void ServiceContainer(this IServiceCollection services)
        {
            services.AddScoped<BlogerRespository>();
            services.AddScoped<TestResposity>();
            services.AddScoped<ProfileRespository>();
            services.AddScoped<SubscribeRepository>();
            services.AddScoped<ArticleRespository>();
            services.AddScoped<TagRepository>();
        }

        public static void Tools(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "My API", Version = "v1" });
            });
        }
    }
}
